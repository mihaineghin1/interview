import React from 'react';

import './Table.Component.scss'

const TableComponent = ({ children }) => (
  <div className="table">
    {Array.isArray(children) ? children.map(component => component) : children}
  </div>
)

export default TableComponent;
