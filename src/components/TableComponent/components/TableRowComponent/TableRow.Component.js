import React, { Component } from 'react';

import './TableRow.Component.scss';
import { TableCell } from '../../..';
import { accessLevels } from '../../../../constants/accessLevels';

class TableRowComponent extends Component {

  state = {
    person_id: '',
    role: '',
    access_level: '',
  }

  componentDidMount = () => {
    const { addMember, member } = this.props;
    this.setState({
      person_id: member.person_id || this.filterActiveMembers(this.props.data)[0].person_id,
      role: member.role || Object.keys(accessLevels)[0],
      access_level: member.access_level || accessLevels[Object.keys(accessLevels)[0]][0],
    }, () => {
      addMember(this.state);
    })
  }

  handleChange = (event) => {
    this.setState({
      ...this.state,
      [event.target.name]: event.target.value
    }, () => {
      this.props.addMember(this.state)
      this.props.shouldSaveOnChange();
    })
  }

  renderNames = () => {
    const { data } = this.props;
    const filteredMembers = this.filterActiveMembers(data)
      .map(({ firstname, lastname, person_id }, index) => {
        return <option key={index} value={person_id}>{`${firstname} ${lastname}`}</option>}
      )
    return filteredMembers;
  }
 
  filterActiveMembers = (data) => {
    const { members, member } = this.props;
    const newData = data.filter(memberApi => !members.some(memberActive =>
      //Filter to not be active
      memberActive.person_id === memberApi.person_id &&
      //Exlude current selected
      memberActive.person_id !== member.person_id
    ));

    return newData;
  }

  deleteMember = () => {
    this.props.deleteMember();
  }

  render() {
    const { member } = this.props;
    return (
      <div className="table-row">
        <TableCell>
          <select
            onChange={this.handleChange}
            value={member.person_id || this.state.person_id} name="person_id"
          >
            {
              this.renderNames()
            }
          </select>
        </TableCell>
        <TableCell>
          <select
            onChange={this.handleChange}
            value={member.role || this.state.role}
            name="role"
          >
            {
              Object.keys(accessLevels)
                .map((key, index) => <option key={index} value={key}>{`${key}`}</option>)
            }
          </select>
        </TableCell>
        <TableCell>
          <select
            onChange={this.handleChange}
            value={member.access_level || this.state.access_level}
            name="access_level"
          >
            {
              this.state.role && accessLevels[`${this.state.role}`]
                .map((level, index) => <option key={index} value={level}>{`${level}`}</option>)
            }
          </select>
        </TableCell>
        <TableCell>
          <button className="delete-member-btn" onClick={this.deleteMember}>
            <img src="https://d1icd6shlvmxi6.cloudfront.net/gsc/5GQ71W/9c/3e/a0/9c3ea0c23cea404fb49d1e89ce31b1e2/images/membership_management_screen/u17.svg?token=e58118d27d9d9d58e2f12039c49e6a047c8e38f9b7211ff66bd661aef0e67b92" />
          </button>
        </TableCell>
      </div>
    )
  }
}

export default TableRowComponent;