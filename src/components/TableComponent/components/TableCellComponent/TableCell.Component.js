import React from 'react';

import './TableCell.Component.scss';

const TableCellComponent = ({ children }) => (
  <div className="table-cell">{children}</div>
)

export default TableCellComponent;
