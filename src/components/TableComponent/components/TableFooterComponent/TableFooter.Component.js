import React from 'react';
import { Button } from '../../..';

const TableFooterComponent = ({ addNewRow }) => (
  <Button mode="text" onClick={addNewRow}>Add new member</Button>
)

export default TableFooterComponent;