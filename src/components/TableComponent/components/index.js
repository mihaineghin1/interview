export { default as TableHeaderComponent } from './TableHeaderComponent/TableHeader.Component';
export { default as TableRowComponent } from './TableRowComponent/TableRow.Component';
export { default as TableCellComponent } from './TableCellComponent/TableCell.Component';
export { default as TableFooterComponent } from './TableFooterComponent/TableFooter.Component';
