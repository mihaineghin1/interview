import React from 'react';

import './TableHeader.Component.scss';

const TableHeaderComponent = ({ children }) => (
  <div className="table-header">
    {Array.isArray(children) ? children.map(component => component) : children}
  </div>
)

export default TableHeaderComponent;