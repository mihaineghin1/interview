import React from 'react';

import './Button.Component.scss';

const ButtonComponent = ({ children, mode, onClick }) => (
  <button className={`button button--${mode}`} onClick={onClick}>
    {children}
  </button>
)

export default ButtonComponent;
