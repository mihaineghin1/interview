import React, { Component } from 'react';

import './AddMember.Component.scss';
import { Button } from '..';

class AddMemberComponent extends Component {

  state = {
    person_id: '',
    firstname: '',
    lastname: '',
    title: '',
    businessUnit: '',
    is_user: false
  }

  componentDidMount = () => {
    document.addEventListener('mousedown', this.handleClickOutside);
  }

  componentWillUnmount = () => {
    document.removeEventListener('mousedown', this.handleClickOutside);
  }

  setWrapperRef = (node) => {
    this.wrapperRef = node;
  }

  handleClickOutside = (event) => {
    const { closeModal } = this.props;

    if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
      closeModal()
    }
  }

  handleChange = (event) => {
    const { name, value, checked, type } = event.target;
    this.setState({ [name]: type === 'checkbox' ? checked : value })
  }



  saveUser = () => {
    const { apiMembers } = this.props;
    const { firstname, lastname } = this.state;

    // Minimal form validation
    if (!firstname.length || !lastname.length) {
      return;
    }

    // Add id for member
    this.setState({person_id: `${firstname}${apiMembers.length}`}, () => this.props.onUserSave(this.state));
  }

  render() {
    const { closeModal } = this.props;

    return (
      <div className="add-member__container">
        <div className="form add-member__form" ref={this.setWrapperRef}>
          <div className="form__header">Create Member</div>
          <div className="form__content">
            <div className="content content__left">
              <div className="input-row">
                <label htmlFor="first-name">First name <span>*</span></label>
                <input required id="first-name" name="firstname" onChange={this.handleChange} />
              </div>
              <div className="input-row">
                <label htmlFor="last-name">Last name <span>*</span></label>
                <input required id="last-name" name="lastname" onChange={this.handleChange} />
              </div>
              <div>
                <input id="is_user" type="checkbox" name="is_user" onChange={this.handleChange} />
                <label htmlFor="is_user">User status</label>
                <p className="subtext">Designates whether the person can login into application</p>
              </div>
            </div>
            <div className="content content__right">
              <div className="input-row">
                <label htmlFor="title">Title</label>
                <input id="title" name="title" onChange={this.handleChange} />
              </div>
              <div className="input-row">
                <label htmlFor="business-unit">Business Unit</label>
                <input id="business-unit" name="businessUnit" onChange={this.handleChange} />
              </div>
            </div>
          </div>
          <div className="form__footer">
            <Button mode="text" onClick={closeModal}>Cancel</Button>
            <Button mode="contained" onClick={this.saveUser}>Create member</Button>
          </div>
        </div>
      </div>
    )
  }
}

export default AddMemberComponent;