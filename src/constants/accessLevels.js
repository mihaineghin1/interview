export const accessLevels = {
  customer: ['read', 'write'],
  employee: ['write', 'admin'],
  manager: ['admin']
};