import React, { Component } from 'react';

import './MbManagement.Container.scss';

import { Table, TableHeader, TableCell, TableRow, TableFooter, Button } from '../../components';
import AddMemberComponent from '../../components/AddMemberComponent/AddMember.Component';

class MbManagementContainer extends Component {

  state = {
    members: [],
    memberFormVisible: false,
    apiMembers: [],
    saveChanges: false
  }

  componentDidMount = async () => {
    const members = localStorage.getItem('members');
    const storageApiMembers = localStorage.getItem('apiMembers');

    const fetchedMembers = await fetch('./members.json', {
      headers: { 
        'Content-Type': 'application/json',
        'Accept': 'application/json'
       }
    })
    const apiMembers = await fetchedMembers.json();

    this.setState({apiMembers: JSON.parse(storageApiMembers) || apiMembers, members: JSON.parse(members) || []})
  }

  addNewRow = () => {
    const { apiMembers } = this.state;
    this.state.members.length < apiMembers.length && this.setState({
      members: [...this.state.members, {}],
      saveChanges: true
    })
  }

  addMember = (member, index) => {
    const newMembers = this.state.members.slice();
    newMembers[index] = member;
    this.setState({
      members: newMembers,
    })
  }

  shouldSaveOnChange = () => {
    this.setState({saveChanges: true});
  }
  
  renderTableRows() {
    const { members, apiMembers } = this.state;
    return members.map((member, index) => (
      <TableRow
        key={index}
        data={apiMembers}
        addMember={(member) => this.addMember(member, index)}
        deleteMember={() => this.deleteMember(index)}
        members={this.state.members}
        member={member}
        shouldSaveOnChange={this.shouldSaveOnChange}
      />
    ))
  }

  openMemberModal = () => {
    this.setState({memberFormVisible: true})
  }

  closeModal = () => {
    this.setState({memberFormVisible: false})
  }

  onUserSave = (user) => {
    this.setState({
      apiMembers: [...this.state.apiMembers, user], memberFormVisible: false
    }, () => localStorage.setItem('apiMembers', JSON.stringify(this.state.apiMembers))
    )
  }

  //Simulate saving changes
  saveChanges = () => {
    localStorage.setItem('members', JSON.stringify(this.state.members));
    this.setState({saveChanges: false});
  }

  deleteMember = (index) => {
    const members = this.state.members.filter((member, memberIndex) => index !== memberIndex);
    this.setState({ members, saveChanges: true })
  }

  render() {
    const { apiMembers, saveChanges } = this.state;
    return (
      <div className="management-container">
        <div className="management-container__content">
          <div className={`members-header ${ saveChanges ? 'members-header--save' : ''}`}>
            <div>
              {!saveChanges && <h3 className="members-header__member-name">Mihai Neghin</h3>}
            </div>
            <div className="header-buttons">
              <Button mode="outlined" onClick={this.openMemberModal}>
                New Member
              </Button>
              {
                saveChanges &&
                  <Button mode="contained" onClick={this.saveChanges}>
                    Save changes
                  </Button>
              }
            </div>
          </div>
          <Table>
            <TableHeader>
              <TableCell>
                Member
              </TableCell>
              <TableCell>
                Role
              </TableCell>
              <TableCell>
                Access Level
              </TableCell>
              <TableCell />
            </TableHeader>
            { 
              !apiMembers.length ? <h4>There are currently no members.</h4> : this.renderTableRows()
            }
            <TableFooter addNewRow={this.addNewRow} />
          </Table>
        </div>
        {
          this.state.memberFormVisible && <AddMemberComponent apiMembers={apiMembers} onUserSave={this.onUserSave} closeModal={this.closeModal} />
        }
      </div>
    )
  }
}

export default MbManagementContainer;
